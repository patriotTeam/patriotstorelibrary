# PatriotStoreLibrary

[![CI Status](http://img.shields.io/travis/ece/PatriotStoreLibrary.svg?style=flat)](https://travis-ci.org/ece/PatriotStoreLibrary)
[![Version](https://img.shields.io/cocoapods/v/PatriotStoreLibrary.svg?style=flat)](http://cocoapods.org/pods/PatriotStoreLibrary)
[![License](https://img.shields.io/cocoapods/l/PatriotStoreLibrary.svg?style=flat)](http://cocoapods.org/pods/PatriotStoreLibrary)
[![Platform](https://img.shields.io/cocoapods/p/PatriotStoreLibrary.svg?style=flat)](http://cocoapods.org/pods/PatriotStoreLibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PatriotStoreLibrary is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PatriotStoreLibrary"
```

## Author

ece, ece@patriothings.com

## License

PatriotStoreLibrary is available under the MIT license. See the LICENSE file for more info.
