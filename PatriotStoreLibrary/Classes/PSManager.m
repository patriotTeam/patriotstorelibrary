//
//  PSManager.m
//  PSGitViewControllers
//
//  Created by EceOzmen on 07/09/16.
//  Copyright © 2016 ece. All rights reserved.
//

#import "PSManager.h"
#import "PSKeychain.h"
@implementation PSManager

@synthesize psAppVersion,psAppID,uuid;

+ (PSManager*)sharedObject {
    
    static dispatch_once_t onceToken;
    static PSManager *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[PSManager alloc] init];
    });
    return instance;

}


- (id)init {
    self = [super init];
    if (self) {
        
        NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
        psAppVersion = infoDictionary[@"CFBundleShortVersionString"];
        uuid = [self getUUID];
        
    }
    return self;
}

- (NSString*)getUUID{
    
    PSKeychain *pskeychain = [[PSKeychain alloc] init];
    
    NSString *uuidstring = [pskeychain stringForKey:@"patriotStoreUUID"];
    if(uuidstring.length == 0)
    {
        NSLog(@"Keychain data not found");
        uuidstring = [[NSUUID UUID] UUIDString];
        [pskeychain addString:uuidstring forKey:@"patriotStoreUUID"];
    }
    
    return uuidstring;
}

@end
