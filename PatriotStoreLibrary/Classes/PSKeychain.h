//
//  PSKeychain.h
//  AHPatients
//
//  Created by EceOzmen on 07/09/16.
//  Copyright © 2016 Patriothings. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSKeychain : NSObject

- (NSString *)stringForKey:(id)key;
- (BOOL) addString:(NSString *)string forKey:(NSString *)key;
- (BOOL) updateString:(NSString *)string forKey:(NSString *)key;
- (BOOL)deleteDataForKey:(id)key;
@end
