//
//  PSManager.h
//  PatriotStorePodProject
//
//  Created by EceOzmen on 08/09/16.
//  Copyright © 2016 Patriothings. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSManager : NSObject{
    NSString * psAppVersion;
    NSString * uuid;
    int psAppID;
}

+ (PSManager*)sharedObject;

@property (nonatomic, retain) NSString* uuid;
@property (nonatomic, retain) NSString* psAppVersion;
@property (nonatomic) int psAppID;


@end
