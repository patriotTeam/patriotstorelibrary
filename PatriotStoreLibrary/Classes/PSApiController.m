//
//  PSApiController.m
//  PSGitViewControllers
//
//  Created by EceOzmen on 07/09/16.
//  Copyright © 2016 ece. All rights reserved.
//

#import "PSApiController.h"
#import "PSManager.h"

#import "AFNetworking.h"
#import "Reachability.h"
#import <SVProgressHUD/SVProgressHUD.h>


#define kDomainName_PatrioStore         @"http://api.patriothings.com"
#define WEB_SERVICE_URL_PATRIOTSTORE(endPoint)   [NSString stringWithFormat:@"%@/PatriotHospitalService.svc/rest/%@", kDomainName_PatrioStore, endPoint]

@implementation PSApiController

+(void)saveAppInfo:(int)appID completionHandler:(void (^)(BOOL isSucceeded, id returnObject))completionHandler{
    
    
    NSMutableDictionary * parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[[PSManager sharedObject] uuid] forKey:@"DeviceId"];
    [parameters setObject:[[PSManager sharedObject] psAppVersion] forKey:@"Version"];
    [parameters setObject:[NSString stringWithFormat:@"%d", appID] forKey:@"ApplicationId"];
    
    [self performPostWithPatriotStoreServiceEndPoint:@"ApplicationVersionOnDeviceSave" parameters:parameters headers:nil body:nil
                                   completionHandler:^(BOOL isSucceeded, id parsedResponse, NSString * errorMessage) {
                                       
                                       if (!isSucceeded) {
                                           [SVProgressHUD showErrorWithStatus:@"Hata!"];
                                           completionHandler(NO,errorMessage);
                                       }
                                       else{
                                           
                                           NSDictionary * responseDict = (NSDictionary *)parsedResponse;
                                           if ([responseDict objectForKey:@"ApplicationVersionOnDeviceSaveResult"]) {
                                               NSDictionary *result = [responseDict objectForKey:@"ApplicationVersionOnDeviceSaveResult"];
                                               if ([[result objectForKey:@"ReturnCode"] intValue]!=99) {
                                                   completionHandler(YES,nil);
                                               }
                                               completionHandler(NO,responseDict);
                                           }
                                       }
                                   }];
    
}

+ (void)performPostWithPatriotStoreServiceEndPoint:(NSString *)serviceEndPoint
                                        parameters:(NSDictionary *)parameters
                                           headers:(NSDictionary *)headers
                                              body:(NSString *)body
                                 completionHandler:(void (^)(BOOL isSucceeded, id parsedResponse, NSString * errorMessage))completionHandler
{
    //NSArray *extractedEndpoints = @[]; // Add endpoints for bg service calls here.
    //BOOL isProgressExtracted = [extractedEndpoints containsObject:serviceEndPoint]? YES : NO;
    
    if (![self checkConnection]) {
        return;
    }
    
    
    NSString * url = WEB_SERVICE_URL_PATRIOTSTORE(serviceEndPoint);
    NSLog(@"Post request started for endPoint: %@", url);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager.requestSerializer setTimeoutInterval:10];
    
    if (parameters)
    {
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        completionHandler(YES,responseObject,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

#pragma mark - Connection Control

+(BOOL)checkConnection{
    if ([self hasConnection]) {
        return YES;
    }
    [SVProgressHUD showErrorWithStatus:@"İnternet bağlantısı bulunamadı!"];
    return NO;
}

+ (BOOL)hasConnection
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}



@end
