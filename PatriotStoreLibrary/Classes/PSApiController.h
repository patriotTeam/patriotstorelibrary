//
//  PSApiController.h
//  PSGitViewControllers
//
//  Created by EceOzmen on 07/09/16.
//  Copyright © 2016 ece. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSApiController : NSObject{
    
}
@property NSString *versionNo;
+(void)saveAppInfo:(int)appID completionHandler:(void (^)(BOOL isSucceeded, id returnObject))completionHandler;

@end
