//
//  PSRegisterDevice.h
//  PSGitViewControllers
//
//  Created by EceOzmen on 07/09/16.
//  Copyright © 2016 ece. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSRegisterDevice : NSObject
- (id)initWithAppID;
@end
