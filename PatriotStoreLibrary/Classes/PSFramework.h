//
//  PSFramework.h
//  PSFramework
//
//  Created by EceOzmen on 08/09/16.
//  Copyright © 2016 Patriothings. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PSFramework.
FOUNDATION_EXPORT double PSFrameworkVersionNumber;

//! Project version string for PSFramework.
FOUNDATION_EXPORT const unsigned char PSFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PSFramework/PublicHeader.h>


