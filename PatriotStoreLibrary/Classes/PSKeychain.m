//
//  PSKeychain.m
//
//  Created by EceOzmen on 07/09/16.
//  Copyright © 2016 Patriothings. All rights reserved.
//

#import "PSKeychain.h"
@interface PSKeychain ()
{
    NSString *service;
}
@end

@implementation PSKeychain

- (NSString *)stringForKey:(id)key
{
    //generate query
    NSMutableDictionary *query = [NSMutableDictionary dictionary];
    if ([service length]) query[(__bridge NSString *)kSecAttrService] = service;
    query[(__bridge NSString *)kSecClass] = (__bridge id)kSecClassGenericPassword;
    query[(__bridge NSString *)kSecMatchLimit] = (__bridge id)kSecMatchLimitOne;
    query[(__bridge NSString *)kSecReturnData] = (__bridge id)kCFBooleanTrue;
    query[(__bridge NSString *)kSecAttrAccount] = [key description];
    
    //recover data
    CFDataRef data = NULL;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)query, (CFTypeRef *)&data);
    if (status != errSecSuccess && status != errSecItemNotFound)
    {
        NSLog(@"Failed to retrieve data for key '%@', error: %ld", key, (long)status);
    }
    
    NSData *dataObj = CFBridgingRelease(data);
    NSString *text = [[NSString alloc] initWithData:dataObj encoding:NSUTF8StringEncoding];
    
    return text;
}


- (BOOL) addString:(NSString *)string forKey:(NSString *)key
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *query = [NSMutableDictionary dictionary];
    if ([service length]) query[(__bridge NSString *)kSecAttrService] = service;
    query[(__bridge NSString *)kSecClass] = (__bridge id)kSecClassGenericPassword;
    query[(__bridge NSString *)kSecAttrAccount] = [key description];
    query[(__bridge NSString *)kSecValueData] = data;
    
    OSStatus status = SecItemAdd ((__bridge CFDictionaryRef)query, NULL);
    
    if (status != errSecSuccess)
    {
        NSLog(@"Failed to add data for key '%@', error: %ld", key, (long)status);
        return NO;
    }
    
    return YES;
}

- (BOOL) updateString:(NSString *)string forKey:(NSString *)key
{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *query = [NSMutableDictionary dictionary];
    if ([service length]) query[(__bridge NSString *)kSecAttrService] = service;
    query[(__bridge NSString *)kSecClass] = (__bridge id)kSecClassGenericPassword;
    query[(__bridge NSString *)kSecAttrAccount] = [key description];
    
    NSMutableDictionary *update = [@{(__bridge NSString *)kSecValueData: data} mutableCopy];
    
    OSStatus status = SecItemUpdate((__bridge CFDictionaryRef)query, (__bridge CFDictionaryRef)update);
    
    if (status != errSecSuccess)
    {
        NSLog(@"Failed to update data for key '%@', error: %ld", key, (long)status);
        return NO;
    }
    
    return YES;
}

- (BOOL)deleteDataForKey:(id)key
{
    //generate query
    NSMutableDictionary *query = [NSMutableDictionary dictionary];
    if ([service length]) query[(__bridge NSString *)kSecAttrService] = service;
    query[(__bridge NSString *)kSecClass] = (__bridge id)kSecClassGenericPassword;
    query[(__bridge NSString *)kSecAttrAccount] = [key description];
    
    //delete existing data
    
    OSStatus status = SecItemDelete((__bridge CFDictionaryRef)query);
    if (status != errSecSuccess)
    {
        NSLog(@"Failed to delete data for key '%@', error: %ld", key, (long)status);
        return NO;
    }
    
    return YES;
}

@end